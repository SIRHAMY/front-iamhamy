---
title: "Connect"
date: 2019-11-13T23:55:21-05:00
---

The best way to keep up with me is through one of my online profiles. Currently most active on [Twitter](https://twitter.com/SIRHAMY).

* Instagram:
    * [@sir.hamy](https://www.instagram.com/sir.hamy/)
    * [@hamy.art](https://www.instagram.com/hamy.art/)
    * [@hamy.labs](https://www.instagram.com/hamy.labs/)
* Twitter: [@SIRHAMY](https://twitter.com/SIRHAMY)
* YouTube: [HAMY LABS](https://www.youtube.com/channel/UCPBY44jxP7gOMkUsP5rlGnw)
* LinkedIn: [Hamilton Greene](https://www.linkedin.com/in/hamiltongreene/)

If you want updates in a more traditional manner, [subscribe to my email list or RSS Feed](/subscribe).

If you're really trying to reach me, shoot me an email at yo [AT] hamy.xyz